package ayekaye.leetcode;

public class LC3 {

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("abcabcbb"));
        System.out.println(lengthOfLongestSubstring("bbbbb"));
        System.out.println(lengthOfLongestSubstring("pwwkew"));
        System.out.println(lengthOfLongestSubstring("abcdefabcdegf"));
    }

    public static int lengthOfLongestSubstring(String s) {
        int max = 0;
        int start = 0;
        int pos = 0;
        for (char currChar : s.toCharArray()) {
            int prev = s.indexOf(currChar, start);
            if (prev >= 0 && prev < pos) {
                start = prev + 1;
            }
            int count = pos - start + 1;
            max = count > max ? count : max;
            pos++;
        }
        return max;
    }
}
