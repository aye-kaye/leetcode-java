package ayekaye.leetcode;

import java.util.concurrent.CountDownLatch;

public class LC1114 {
    public static void main(String[] args) {
        final LC1114 o = new LC1114();
        new Thread(() -> {
            try {
                o.first(() -> System.out.println("first"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                o.third(() -> System.out.println("third"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                o.second(() -> System.out.println("second"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private CountDownLatch l2 = new CountDownLatch(1);
    private CountDownLatch l3 = new CountDownLatch(1);

    public LC1114() {
    }

    public void first(Runnable printFirst) throws InterruptedException {
        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
        l2.countDown();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        l2.await();
        // printSecond.run() outputs "second". Do not change or remove this line.
        printSecond.run();
        l3.countDown();
    }

    public void third(Runnable printThird) throws InterruptedException {
        l3.await();
        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
    }
}
