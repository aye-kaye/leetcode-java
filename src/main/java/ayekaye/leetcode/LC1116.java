package ayekaye.leetcode;

import java.util.concurrent.Semaphore;
import java.util.function.IntConsumer;

public class LC1116 {
    private int n;

    private Semaphore sz = new Semaphore(1);
    private Semaphore se = new Semaphore(0);
    private Semaphore so = new Semaphore(0);

    public LC1116(int n) {
        this.n = n;
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public void zero(IntConsumer printNumber) throws InterruptedException {
        for (int i = 1; i <= n; i++) {
            sz.acquire();
            printNumber.accept(0);
            if (i % 2 == 1) {
                so.release();
            } else {
                se.release();
            }
        }
    }

    public void even(IntConsumer printNumber) throws InterruptedException {
        for (int i = 2; i <= n; i = i + 2) {
            se.acquire();
            printNumber.accept(i);
            sz.release();
        }
    }

    public void odd(IntConsumer printNumber) throws InterruptedException {
        for (int i = 1; i <= n; i = i + 2) {
            so.acquire();
            printNumber.accept(i);
            sz.release();
        }
    }

}
