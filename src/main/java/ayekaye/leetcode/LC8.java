package ayekaye.leetcode;

/**
 * @author: aleksey.kondrashkov
 */
public class LC8 {
    public int myAtoi(String str) {
        int pos = 0;
        int start = -1;
        boolean valid = false;
        while (pos++ < str.length()) {
            char c = str.charAt(pos - 1);
            if (start < 0) {
                if (Character.isSpaceChar(c)) {
                    continue;
                } else if (c == '-' || c == '+') {
                    start = pos - 1;
                } else if (Character.isDigit(c)) {
                    start = pos - 1;
                    valid = true;
                } else {
                    break;
                }
            } else if (Character.isDigit(c)) {
                valid = true;
            } else {
                break;
            }
        }

        try {
            return valid ? Integer.valueOf(str.substring(start, pos - 1)) : 0;
        } catch (NumberFormatException e) {
            int minus = str.indexOf('-');
            return (minus >= 0 && minus < pos-1) ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        }
    }
}
