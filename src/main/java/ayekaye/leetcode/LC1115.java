package ayekaye.leetcode;

import java.util.concurrent.Semaphore;

public class LC1115 {
    private int n;

    private static Semaphore s1 = new Semaphore(1);
    private static Semaphore s2 = new Semaphore(1);

    public LC1115(int n) {
        this.n = n;
        s2.tryAcquire();
    }

    public void foo(Runnable printFoo) throws InterruptedException {

        for (int i = 0; i < n; i++) {
            s1.acquire();
            // printFoo.run() outputs "foo". Do not change or remove this line.
            printFoo.run();
            s2.release();
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {

        for (int i = 0; i < n; i++) {
            s2.acquire();
            // printBar.run() outputs "bar". Do not change or remove this line.
            printBar.run();
            s1.release();
        }
    }
}
