package ayekaye.leetcode;

/**
 * @author: aleksey.kondrashkov
 */
public class LC2 {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = null;
        ListNode next = null;
        int ovf = 0;
        while (l1 != null || l2 != null || ovf > 0) {
            int v1 = l1 != null ? l1.val : 0;
            int v2 = l2 != null ? l2.val : 0;
            int res = ovf + v1 + v2;

            ListNode n = new ListNode(res % 10);
            if (head == null) {
                head = n;
            } else {
                next.next = n;
            }
            next = n;
            ovf = (int) (res / 10);
            l1 = l1 != null ? l1.next : null;
            l2 = l2 != null ? l2.next : null;
        }
        return head;
    }

    //Definition for singly-linked list.
    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
