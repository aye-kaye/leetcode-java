package ayekaye.leetcode;

public class LC993 {

    public boolean isCousins(TreeNode root, int x, int y) {
        TraverseContext ctx = new TraverseContext(x, y);
        return traverse(root, ctx, 0, 0);
    }

    public boolean traverse(TreeNode node, TraverseContext ctx, int parentVal, int depth) {
        // We found one of the nodes needed. Then check:
        if (node.val == ctx.x || node.val == ctx.y) {
            // - whether it is deep enough to be a cousin
            if (depth < 2) {
                return false;
            // - and if it's the second node found (maxDepth filled with the first node's depth) it satisfies other conditions:
            //   - it's on the same depth
            //   - is not under the same parent
            } else if (ctx.maxDepth > 0
                    && (ctx.maxDepth != depth || parentVal == ctx.lastParent)) {
                return false;
            }
            // it's the first node found - save its depth and parent value
            ctx.maxDepth = depth;
            ctx.lastParent = parentVal;
        }

        // Check left and right subtrees
        if (node.left != null
                && ! traverse(node.left, ctx, node.val, depth + 1) ) {
            return false;
        }
        if (node.right != null
            && ! traverse(node.right, ctx, node.val, depth + 1)) {
            return false;
        }

        return true;
    }

    // Store long term context for the traversal
    public static class TraverseContext {
        // Init with required nodes' values
        final int x;
        final int y;
        // Depth of the first node found
        int maxDepth;
        // and its parent value
        int lastParent;

        public TraverseContext(int x, int y) {
            this.x = x;
            this.y = y;
        }

    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
