package ayekaye.leetcode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class LC15 {

    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        int len = nums.length;

        Map<Integer, Triplet> triplets = new HashMap<>();
        for (int i = 0; i < len - 2; i++) {
            int a = nums[i];
            int start = i + 1;
            int end = len - 1;
            while (start < end) {
                int b = nums[start];
                int c = nums[end];
                int sum = a + b + c;
                if (sum == 0) {
                    Triplet t = new Triplet(a, b, c);
                    int hash = t.hashCode();
                    if (!triplets.containsKey(hash)) {
                        triplets.put(hash, t);
                    }
                    start++;
                    end--;
                } else if (sum > 0) {
                    end--;
                } else {
                    start++;
                }
            }
        }
        return new TreeSet<>(triplets.values())
                .stream()
                .map(Triplet::toList)
                .collect(Collectors.toList());
    }

    public static final class Triplet implements Comparable<Triplet> {
        final Integer a;
        final Integer b;
        final Integer c;

        public Triplet(Integer a, Integer b, Integer c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public Integer getA() {
            return a;
        }

        public Integer getB() {
            return b;
        }

        public Integer getC() {
            return c;
        }

        public List<Integer> toList() {
            return List.of(a, b, c);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Triplet triplet = (Triplet) o;
            return a.equals(triplet.a) && b.equals(triplet.b) && c.equals(triplet.c);
        }

        @Override
        public int hashCode() {
            return Objects.hash(a, b, c);
        }

        @Override
        public int compareTo(Triplet o) {
            return Comparator
                    .comparingInt(Triplet::getA)
                    .thenComparingInt(Triplet::getB)
                    .thenComparingInt(Triplet::getC)
                    .compare(this, o);
        }
    }

}
