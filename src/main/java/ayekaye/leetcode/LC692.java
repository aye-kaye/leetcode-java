package ayekaye.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.LongAdder;

public class LC692 {
    public List<String> topKFrequent(String[] words, int k) {
        ConcurrentMap<String, LongAdder> m = new ConcurrentHashMap<>();
        Arrays.stream(words).parallel().forEach(word ->
            m.computeIfAbsent(word, w -> new LongAdder()).increment()
        );

        Queue<Map.Entry<String, LongAdder>> q = new PriorityQueue<>(m.size(), new KWordsComparator());
        q.addAll(m.entrySet());
        List<String> list = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            Map.Entry<String, LongAdder> e = q.poll();
            if (e == null) {
                break;
            }
            list.add(e.getKey());
        }

        return list;
    }

    public static void main(String[] args) {
        LC692 lc = new LC692();
        String[] words = {"abc", "xyz", "abc", "o", "o"};
        System.out.println(lc.topKFrequent(words, 5));
    }

    public static final class KWordsComparator implements Comparator<Map.Entry<String, LongAdder>> {
        @Override
        public int compare(Map.Entry<String, LongAdder> e1, Map.Entry<String, LongAdder> e2) {
            int ccmp = Integer.compare(e2.getValue().intValue(), e1.getValue().intValue());
            return ccmp == 0
                    ? String.CASE_INSENSITIVE_ORDER.compare(
                    e1.getKey(),
                    e2.getKey())
                    : ccmp;
        }
    }
}
