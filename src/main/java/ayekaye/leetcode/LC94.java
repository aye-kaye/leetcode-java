package ayekaye.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LC94 {
    public List<Integer> inorderTraversalRec(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        } else if (!hasChildren(root)) {
            return List.of(root.val);
        } else {
            List<Integer> collector = new ArrayList<>(inorderTraversalRec(root.left));
            collector.add(root.val);
            collector.addAll(inorderTraversalRec(root.right));
            return collector;
        }
    }

    public List<Integer> inorderTraversal(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        } else {
            List<Integer> collector = new ArrayList<>();
            TreeNode current = root;
            Deque<TreeNode> q = new LinkedList<>();
            // null - non-visited node; left - left was checked or empty; right - right was checked or empty - we can pop parent node now.
            Map<Integer, Boolean> dirMap = new HashMap<>();
            while (current != null) {
                int val = current.val;
                Boolean dir = dirMap.get(val);
                if (dir == null) {
                    if (current.left != null) {
                        dirMap.put(val, false);
                        q.add(current);
                        current = current.left;
                        continue;
                    } else {
                        dir = false;
                        dirMap.put(val, dir);
                    }
                }
                if (!dir) {
                    collector.add(val);
                    dirMap.put(val, true);
                    if (current.right != null) {
                        q.add(current);
                        current = current.right;
                        continue;
                    }
                }
                dirMap.remove(val);
                current = q.pollLast();
            }
            return collector;
        }
    }

    public static boolean hasChildren(TreeNode n) {
        return n != null
                && (n.left != null
                || n.right != null);
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
