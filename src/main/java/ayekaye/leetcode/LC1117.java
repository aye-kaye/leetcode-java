package ayekaye.leetcode;

import java.util.concurrent.Phaser;

public class LC1117 {
    private final Phaser parent = new Phaser(0) {
        @Override
        protected boolean onAdvance(int phase, int registeredParties) {
            boolean res = super.onAdvance(phase, registeredParties);
            System.out.printf("Doing advance %d %d %s%n", oPhaser.getPhase(), hPhaser.getPhase(), Thread.currentThread().getName());
            return res;
        }
    };

    private final Phaser oPhaser = new Phaser(parent, 1);
    private final Phaser hPhaser = new Phaser(parent, 2);

    public void hydrogen(Runnable releaseHydrogen) throws InterruptedException {
//        int arrive = hPhaser.arrive();
//        System.out.println("H phase " + arrive);
//        hPhaser.awaitAdvance(arrive);
        hPhaser.arriveAndAwaitAdvance();
        System.out.printf("H [%d] '%b' %s%n", hPhaser.getUnarrivedParties(), hPhaser.isTerminated(), Thread.currentThread().getName());
        // releaseHydrogen.run() outputs "H". Do not change or remove this line.
        releaseHydrogen.run();
    }

    public void oxygen(Runnable releaseOxygen) throws InterruptedException {
//        int arrive = parent.arrive();
//        System.out.println("O phase " + arrive);
//        oPhaser.awaitAdvance(arrive);
        oPhaser.arriveAndAwaitAdvance();
//        oPhaser.arriveAndAwaitAdvance();
        System.out.printf("O [%d] '%b' %s%n", oPhaser.getUnarrivedParties(), oPhaser.isTerminated(), Thread.currentThread().getName());
        // releaseOxygen.run() outputs "O". Do not change or remove this line.
        releaseOxygen.run();
    }
}
