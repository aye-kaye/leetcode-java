package ayekaye.leetcode;

import java.util.ArrayList;
import java.util.List;

public class LC38 {
    public String countAndSay(int n) {
        List<Byte> arr = iter(n, 1, List.of((byte) 1));
        StringBuilder sb = new StringBuilder();
        for (Byte b : arr) {
            sb.append(b);
        }
        return sb.toString();
    }

    public List<Byte> iter(int limit, int pos, List<Byte> arr) {
        if (pos < limit) {
            List<Byte> newArr = new ArrayList<>();
            byte prev = arr.get(0);
            byte count = 0;
            for (int i = 0; i < arr.size(); i++) {
                byte b = arr.get(i);
                boolean changed = prev != b;
                if (!changed) {
                    count++;
                }
                if (changed) {
                    newArr.add(count);
                    newArr.add(prev);
                    count = 1;
                }
                if (i == arr.size() - 1) {
                    newArr.add(count);
                    newArr.add(b);
                }
                prev = b;
            }
            return iter(limit, pos + 1, newArr);
        } else {
            return arr;
        }
    }

    public static void main(String[] args) {
        LC38 lc = new LC38();
        System.out.println(lc.countAndSay(1));
        System.out.println(lc.countAndSay(4));
        System.out.println(lc.countAndSay(5));
    }
}
