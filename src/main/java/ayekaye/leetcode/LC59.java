package ayekaye.leetcode;

public class LC59 {

    public static void main(String[] args) {
        int s = 3;
        int[][] m = new LC59().generateMatrix(s);
        for (int r = 0; r < s; r++) {
            for (int c = 0; c < s; c++) {
                System.out.printf("%2d ", m[c][r]);
            }
            System.out.println();
        }
    }

    public int[][] generateMatrix(int n) {
        int[][] m = new int[n][n];

         // 0 - right, 1 - down, 2 - left, 3 - up
        int dir = 0;
        int pos = 1;
        int limit = n * n;
        m[0][0] = 1;
        int x = 0;
        int y = 0;
        int el = 3;
        for (int e = n - 1; e > 0; e--) {
            if (e < n - 1) {
                el = 2;
            }
            for (int t = 0; t < el; t++) {
                for (int j = 0; j < e; j++) {
                    if (++pos > limit) {
                        return m;
                    }
                    if (dir == 0) {
                        m[x][++y] = pos;
                    } else if (dir == 1) {
                        m[++x][y] = pos;
                    } else if (dir == 2) {
                        m[x][--y] = pos;
                    } else {
                        m[--x][y] = pos;
                    }
                }
                dir = dir < 3 ? dir + 1 : 0;
            }
        }
        return m;
    }
}
