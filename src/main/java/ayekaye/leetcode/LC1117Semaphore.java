package ayekaye.leetcode;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class LC1117Semaphore {

    private final Semaphore hSemaphore = new Semaphore(2);
    private final Semaphore oSemaphore = new Semaphore(1);
    private final CyclicBarrier oBarrier = new CyclicBarrier(3, () -> {
        oSemaphore.release();
        hSemaphore.release(2);
    });

    public void hydrogen(Runnable releaseHydrogen) throws InterruptedException {
        hSemaphore.acquire();
        System.out.printf("H %s%n", Thread.currentThread().getName());
        // releaseHydrogen.run() outputs "H". Do not change or remove this line.
        releaseHydrogen.run();

        try {
            oBarrier.await();
        } catch (BrokenBarrierException e) {
            throw new InterruptedException(e.getMessage());
        }
    }

    public void oxygen(Runnable releaseOxygen) throws InterruptedException {
        oSemaphore.acquire();
        System.out.printf("O %s%n", Thread.currentThread().getName());
        // releaseOxygen.run() outputs "O". Do not change or remove this line.
        releaseOxygen.run();
        try {
            oBarrier.await();
        } catch (BrokenBarrierException e) {
            throw new InterruptedException(e.getMessage());
        }
    }
}
