package ayekaye.leetcode;

import java.util.Arrays;

public class LC11 {
    public int maxArea(int[] height) {

        int[] vol = new int[height.length];
        int prevHiIdx = -1;
        int prevHi = -1;
        for (int i = 1; i < height.length; i++) {
            prevHiIdx = prevHiIdx < 0 ?
                    (height[i - 1] > 0 ? i - 1 : -1) :
                    (height[i - 1] >= height[prevHiIdx] ? (i - 1) : prevHiIdx);
            if (prevHiIdx < 0) {
                continue;
            }

            prevHi = height[prevHiIdx];

            int h = height[i];
            for (int j = i - 1; j >= prevHiIdx; j--) {
                if (vol[j] < h) {
                    vol[j] = Math.min(prevHi, h);
                } else {
                    break;
                }
            }
        }

        System.out.println(Arrays.toString(vol));
        return 0;
    }

}
