package ayekaye.leetcode;

public class LC12 {
    public static void main(String[] args) {
        System.out.println(intToRoman(3));
        System.out.println(intToRoman(4));
        System.out.println(intToRoman(9));
        System.out.println(intToRoman(58));
        System.out.println(intToRoman(1994));
    }

    public static String intToRoman(int num) {
        String[][] voc = new String[][] { {"I", "V", "X"},
                                          {"X", "L", "C"},
                                          {"C", "D", "M"},
                                          {"M", "", ""}
        };
        String[] fmt = {
                "",
                "%1$s",
                "%1$s%1$s",
                "%1$s%1$s%1$s",
                "%1$s%2$s",
                "%2$s",
                "%2$s%1$s",
                "%2$s%1$s%1$s",
                "%2$s%1$s%1$s%1$s",
                "%1$s%3$s"
        };

        int it = 0;
        StringBuilder sb = new StringBuilder();
        while (num > 0) {
            int rem = num % 10;
            if (rem != 0) {
                sb.insert(0, String.format(fmt[rem], voc[it][0], voc[it][1], voc[it][2]));
            }
            num = num / 10;
            it++;
        }
        return sb.toString();
    }
}
