package ayekaye.leetcode;

/**
 * @author: aleksey.kondrashkov
 */
public class LC9 {
    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        String s = String.format("%d", x);
        int len = (byte) s.length();
        int p1 = len - 1;
        int p2 = 0;
        int half = len % 2 == 0 ? len / 2 : (len - 1) / 2;
        for (int i = 0; i < half; i++, p1--, p2++) {
            if (s.charAt(p1) != s.charAt(p2)) {
                return false;
            }
        }
        return true;
    }
}
