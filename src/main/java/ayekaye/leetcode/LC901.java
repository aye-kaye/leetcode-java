package ayekaye.leetcode;

import java.util.Deque;
import java.util.LinkedList;

public class LC901 {
    private final Deque<Span> stack = new LinkedList<>();

    public LC901() {
    }

    public int next(int price) {
        int span = 1;
        if (stack.size() == 0) {
            stack.add(new Span(price, 1));
            return 1;
        }

        while (stack.size() > 0 && stack.peek().getPrice() <= price) {
            span += stack.pop().getSpan();
        }

        stack.push(new Span(price, span));
        return span;
    }

    public static final class Span {
        private final int price;
        private final int span;

        public Span(int price, int span) {
            this.price = price;
            this.span = span;
        }

        public int getPrice() {
            return price;
        }

        public int getSpan() {
            return span;
        }
    }
}
