package ayekaye.leetcode;

import java.util.Iterator;

public class LC284 implements Iterator<Integer> {
    private final Iterator<Integer> iterator;
    private int next = -1;

    public LC284(Iterator<Integer> iterator) {
        // initialize any member here.
        this.iterator = iterator;
        if (iterator.hasNext()) {
            next = iterator.next();
        }
    }

    // Returns the next element in the iteration without advancing the iterator.
    public Integer peek() {
        return next;
    }

    // hasNext() and next() should behave the same as in the Iterator interface.
    // Override them if needed.
    @Override
    public Integer next() {
        int ret = next;
        next = iterator.hasNext() ? iterator.next() : -1;
        return ret;
    }

    @Override
    public boolean hasNext() {
        return next >= 0;
    }
}
