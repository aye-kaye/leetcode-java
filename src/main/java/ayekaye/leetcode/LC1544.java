package ayekaye.leetcode;

public class LC1544 {
    public String makeGood(String s) {
        StringBuilder sb = new StringBuilder(s);

        int start = 0;
        int end = s.length() - 1;
        while (start < end) {
            char first = sb.charAt(start);
            char second = sb.charAt(start + 1);
            if (((int) first > (int) second && first == Character.toLowerCase(second)) ||
                ((int) second > (int) first && second == Character.toLowerCase(first))
            ) {
                sb.delete(start, start + 2);
                start = Integer.max(0, start - 1);
                end = sb.length() - 1;
            } else {
                start++;
            }
        }
        return sb.toString();
    }
}
