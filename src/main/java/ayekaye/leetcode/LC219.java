package ayekaye.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LC219 {
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        if (k == 0) {
            return false;
        }
        Map<Integer, Integer> distanceMap = new HashMap<>();

        for (int start = 0; start < nums.length; start++) {
            int val = nums[start];
            int pos = start;
            if (Integer.MIN_VALUE == distanceMap.compute(val, (_k, old) -> {
                int len = old == null ? 0 : Math.abs(pos - old);
                if (len > k || len == 0) {
                    return pos;
                } else {
                    return Integer.MIN_VALUE;
                }
            })) {
                return true;
            };
            if (start >= k) {
                distanceMap.remove(nums[start - k]);
            }
        }

        return false;
    }

    public static void main(String[] args) {
        LC219 lc = new LC219();
        boolean r1 = lc.containsNearbyDuplicate(new int[] {1,2,3,1}, 3);
        boolean r2 = lc.containsNearbyDuplicate(new int[] {1,0,1,1}, 1);
        boolean r3 = lc.containsNearbyDuplicate(new int[] {1,2,3,1,2,3}, 2);
        boolean r4 = lc.containsNearbyDuplicate(new int[] {99,99}, 2);
        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);
        System.out.println(r4);
    }
}
