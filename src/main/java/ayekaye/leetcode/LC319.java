package ayekaye.leetcode;

import java.util.BitSet;

public class LC319 {
    public static void main(String[] args) {
        assert bulbSwitch(3) == 1 : "3";
        assert bulbSwitch(5) == 2 : "2";
        assert bulbSwitch(16) == 4 : "4";
    }

    public static int bulbSwitch(int n) {
        if (n < 1) {
            return 0;
        }
        int i = 1;
        int sqr = 1;
        for (; sqr >= 0 && sqr <= n; i++, sqr = i*i) {
        }
        return i - 1;
    }

}
