package ayekaye.leetcode;

import java.util.*;
import java.util.stream.Collectors;

public class LC2092 {
    public static final class TimeSlot implements Comparable<TimeSlot> {
        private final Integer timeslot;
        private final Map<Integer, Set<Integer>> groupMap = new HashMap<>();

        public TimeSlot(Integer timeslot) {
            this.timeslot = timeslot;
        }

        public Integer getTimeslot() {
            return timeslot;
        }

        public boolean isPresent(Integer person) {
            return groupMap.containsKey(person);
        }

        public Set<Integer> get(Set<Integer> persons) {
            return groupMap.entrySet()
                    .stream()
                    .filter(entry -> persons.contains(entry.getKey()))
                    .map(Map.Entry::getValue)
                    .reduce(new HashSet<>(), (acc, group) -> { acc.addAll(group); return acc; } );
        }

        public void join(Integer person1, Integer person2) {
            if (groupMap.containsKey(person1)) {
                if (groupMap.containsKey(person2)) {
                    merge(person1, person2);
                } else {
                    add(person1, person2);
                }
            } else {
                if (groupMap.containsKey(person2)) {
                    add(person2, person1);
                } else {
                    put(person1, person2);
                }
            }
        }

        private void put(Integer person1, Integer person2) {
            Set<Integer> set1 = new HashSet<>(Set.of(person1, person2));
            groupMap.put(person1, set1);
            groupMap.put(person2, set1);
        }

        private void add(Integer person1, Integer person2) {
            Set<Integer> set1 = groupMap.get(person1);
            set1.add(person2);
            groupMap.put(person2, set1);
        }

        private void merge(Integer person1, Integer person2) {
            Set<Integer> set1 = groupMap.get(person1);
            Set<Integer> set2 = groupMap.get(person2);
            if (set1.equals(set2)) {
                return;
            }
            set1.addAll(set2);
            groupMap.put(person2, set1);
        }

        @Override
        public int compareTo(TimeSlot other) {
            return Integer.compare(this.timeslot, other.getTimeslot());
        }
    }

    public List<Integer> findAllPeople(int n, int[][] meetings, int firstPerson) {
        Map<Integer, TimeSlot> meetingsMap = new TreeMap<>();
        // Build map
        for (int[] triple : meetings) {
            int timeslot = triple[2];
            TimeSlot ts = null;
            if (meetingsMap.containsKey(timeslot)) {
                ts = meetingsMap.get(timeslot);
            } else {
                ts = new TimeSlot(timeslot);
                meetingsMap.put(timeslot, ts);
            }
            ts.join(triple[0], triple[1]);
        }

        // Find connections
        Set<Integer> connected = new TreeSet<>();
        connected.add(0);
        connected.add(firstPerson);
        for (TimeSlot ts : meetingsMap.values()) {
//            if (ts.isPresent(firstPerson)) {
//                connected.add(firstPerson);
//            }
            Set<Integer> newGroup = ts.get(connected);
            if (!newGroup.isEmpty()) {
                connected.addAll(newGroup);
            }
        }
        return List.copyOf(connected);
    }
}
