package ayekaye.leetcode;

public class LC42 {
    public int trap(int[] height) {
        int[] vol = new int[height.length];
        int prevHiIdx = -1;
        int prevHi = -1;
        for (int i = 1; i < height.length; i++) {
            prevHiIdx = prevHiIdx < 0 ?
                    (height[i - 1] > 0 ? i - 1 : -1) :
                    (height[i - 1] >= height[prevHiIdx] ? (i - 1) : prevHiIdx);
            if (prevHiIdx < 0) {
                continue;
            }

            prevHi = height[prevHiIdx];

            int h = height[i];
            for (int j = i - 1; j >= prevHiIdx; j--) {
                if (vol[j] < h) {
                    vol[j] = Math.min(prevHi, h);
                } else {
                    break;
                }
            }
        }

        int count = 0;
        for (int i = 0; i < vol.length - 1; i++) {
            count += Math.max(vol[i] - height[i + 1], 0);
        }
        return count;
    }
}
