package ayekaye.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LC386 {
    public static void main(String[] args) {
        System.out.println(lexicalOrder(5_000_000));
    }

    public static List<Integer> lexicalOrder(int n) {
        if (n <= 0) {
            return Collections.emptyList();
        }
        int length = (int) Math.log10(n) + 1;
        int[] base = new int[length];
        base[0] = 1;

        List<Integer> res = new ArrayList<>();
        int pos = 0;
        boolean ovf = false;
        int ibase = 0;
        while (pos >= 0) {
            ibase = baseToInt(base, pos + 1);

            if (pos == length - 1) {
                for (int i = 0; ibase <= n && i < 10; ) {
                    res.add(ibase);

                    i++;
                    ibase++;
                }
                pos--;
                ovf = true;
            } else {
                if (ovf) {
                    int val = base[pos];
                    if (val == 9) {
                        base[pos] = 0;
                        pos--;
                        ovf = true;
                    } else {
                        base[pos] = val + 1;
                        ovf = false;
                    }
                } else {
                    res.add(ibase);
                    pos++;
                }
            }
        }

        return res;
    }

    public static int baseToInt(int[] base, int digits) {
        int ibase = 0;
        for (int d = 0; d < digits; d++) {
            ibase += base[d] * (int) Math.pow(10, digits - d - 1);
        }
        return ibase;
    }

}
