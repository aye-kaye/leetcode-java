package ayekaye.leetcode;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class L28 {
    private static final int NOT_FOUND_IDX = -1;

    public int strStr(String haystack, String needle) {
        if (haystack.length() < needle.length() || needle.isEmpty()) {
            return NOT_FOUND_IDX;
        }

        byte initialByte = needle.substring(0, 1).getBytes(StandardCharsets.US_ASCII)[0];
        ByteBuffer hbytes = ByteBuffer.wrap(haystack.getBytes(StandardCharsets.US_ASCII));
        ByteBuffer view = hbytes.duplicate();
        ByteBuffer nbytes = ByteBuffer.wrap(needle.getBytes(StandardCharsets.US_ASCII));
        int nsize = nbytes.limit();

        for (int pos = 0; pos < haystack.length() - needle.length() + 1; pos++) {
            if (hbytes.get() == initialByte) {
                view.limit(pos + nsize);
                view.position(pos);
                if (view.compareTo(nbytes) == 0) {
                    return pos;
                }
            }
        }
        return NOT_FOUND_IDX;
    }
}
