package ayekaye.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LC19 {
    public static void main(String[] args) {
        System.out.println( letterCombinations("7") );
        System.out.println( letterCombinations("23456789") );
        System.out.println( letterCombinations("29") );
        System.out.println( letterCombinations("234") );
    }

    static final char[][] chars = new char[][] {
            {'a', 'b', 'c'},
            {'d', 'e', 'f'},
            {'g', 'h', 'i'},
            {'j', 'k', 'l'},
            {'m', 'n', 'o'},
            {'p', 'q', 'r', 's'},
            {'t', 'u', 'v'},
            {'w', 'x', 'y', 'z'}
    };

    public static List<String> letterCombinations(String digits) {
        if (digits == null || digits.isEmpty()) {
            return Collections.emptyList();
        }
        List<String> res = new ArrayList<>();
        final int length = digits.length();
        int[] pos = new int[length];
        final int[] indices = digits.chars().mapToObj(i -> (char) i).mapToInt(ch -> Character.digit(ch, 10) - 2).toArray();
        StringBuilder sb = new StringBuilder();
        while (true) {
            sb.delete(0, sb.length());
            for (int i = 0; i < length; i++) {
                sb.append(chars[indices[i]][pos[i]]);
            }
            res.add(sb.toString());

            for (int j = length - 1; j >= 0; j--) {
                pos[j] = pos[j] + 1;

                if (pos[j] < chars[indices[j]].length) {
                    break;
                }
                if (j == 0) {
                    return res;
                }
                pos[j] = 0;
            }
        }
    }
}
