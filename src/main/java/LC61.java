public class LC61 {

    public static void main(String[] args) {
        ListNode input = new ListNode(1);
        input.next = new ListNode(2);
        input.next.next = new ListNode(3);
        input.next.next.next = new ListNode(4);
        input.next.next.next.next = new ListNode(5);
        input.next.next.next.next.next = new ListNode(6);
        input.next.next.next.next.next.next = new ListNode(7);

        ListNode res = rotateRight(input, 3);
        System.out.println(toString(res));
    }


    public static String toString(ListNode head) {
        StringBuilder sb = new StringBuilder();
        while (head != null) {
            sb.append(String.format("%d->", head.val));
            head = head.next;
        }
        sb.append("null");
        return sb.toString();
    }

    public static ListNode rotateRight(ListNode head, int k) {
        if (k <= 0 || head == null || head.next == null) {
            return head;
        }

        ListNode next = head;
        // scan previous tail at the distance of k from current
        ListNode prevTail = null;
        int length = 0;
        while (true) {
            length++;
            if (prevTail != null) {
                prevTail = prevTail.next;
            } else if (length == k + 1) {
                prevTail = head;
            }


            if (next.next == null) {

                if (length < k) {
                    int shift = length - k % length;
                    // if k divides evenly by length head remains the same
                    if (shift == length) {
                        return head;
                    }

                    prevTail = head;
                    // scan the remainder nodes from the tail = (length - remainder) nodes from head
                    for (int i = 1; i < shift; i++) {
                        prevTail = prevTail.next;
                    }

                    return reinject(prevTail, head);

                } else if (length == k) {
                    // list rotates entirely so head remains the same
                    return head;

                } else {
                    return reinject(prevTail, head);
                }
            }
            next = next.next;
        }
    }

    public static ListNode reinject(ListNode prevTail, ListNode head) {
        ListNode newHead = prevTail.next;
        prevTail.next = null;

        ListNode next = newHead;
        while (true) {
            if (next.next == null) {
                next.next = head;
                return newHead;
            }
            next = next.next;
        }
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
