package ayekaye.leetcode;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class LC15Test {

    @Test
    public void test() {
        LC15 runner = new LC15();
        {
            int[] nums = {-1, 0, 1, 2, -1, -4};
            List<List<Integer>> res1 = runner.threeSum(nums);
            Integer[][] arr1 = res1.stream().map(integers -> integers.toArray(new Integer[] {})).toArray(Integer[][]::new);
            System.out.println(res1);
            assertArrayEquals(
                    new Integer[][] {new Integer[] {-1, -1, 2}, new Integer[] {-1, 0, 1} },
                    arr1);
        }
        {
            int[] nums2 = {-99822, -99927, 21, 2246, 97681, 12687, 87240, 14861, 85066, 99801};
            List<List<Integer>> res2 = runner.threeSum(nums2);
            Integer[][] arr2 = res2.stream().map(integers -> integers.toArray(new Integer[] {})).toArray(Integer[][]::new);
            System.out.println(res2);
            assertArrayEquals(
                    new Integer[][] {new Integer[] {-99927,2246,97681}, new Integer[] {-99927,12687,87240}, new Integer[] {-99927,14861,85066}, new Integer[] {-99822, 21, 99801} },
                    arr2);
        }
    }
}
