package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LC38Test {
    LC38 lc38 = new LC38();

    @Test
    public void basicTest() {
        Assertions.assertEquals("1", lc38.countAndSay(1));
        Assertions.assertEquals("1211", lc38.countAndSay(4));
        Assertions.assertEquals("111221", lc38.countAndSay(5));
    }
}
