package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class L28Test {

    @Test
    public void test() {
        L28 runner = new L28();
        {
            String h1 = "abcde";
            String n1 = "bcd";
            int pos1 = runner.strStr(h1, n1);
            Assertions.assertEquals(1, pos1);
        }
        {
            String h2 = "aaaaaabcde";
            String n2 = "bcd";
            int pos2 = runner.strStr(h2, n2);
            Assertions.assertEquals(6, pos2);
        }
        {
            String h3 = "aaaaaabcde";
            String n3 = "cdef";
            int pos3 = runner.strStr(h3, n3);
            Assertions.assertEquals(-1, pos3);
        }
        {
            String h4 = "a";
            String n4 = "a";
            int pos4 = runner.strStr(h4, n4);
            Assertions.assertEquals(0, pos4);
        }
        {
            String h5 = "aabaaaababaababaa";
            String n5 = "bbbb";
            int pos5 = runner.strStr(h5, n5);
            Assertions.assertEquals(-1, pos5);
        }
    }
}
