package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LC42Test {

    private final static LC42 L = new LC42();

    @Test
    public void test() {


        int[] t1 = {0,1,0,2,1,0,1,3,2,1,2,1};
        Assertions.assertEquals(6, L.trap(t1));

        int[] t2 = {4,2,0,3,2,5};
        Assertions.assertEquals(9, L.trap(t2));
    }
}
