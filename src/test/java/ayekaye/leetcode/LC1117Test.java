package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class LC1117Test {
    @Test
    public void runner() {
        test("HOH");
        test("OOHHHH");
        test("OOOOOHHHHHHHHHH");
    }

    public void test(String input) {
        LC1117Semaphore lc = new LC1117Semaphore();
//        LC1117 lc = new LC1117();
        ConcurrentLinkedQueue<Character> q = new ConcurrentLinkedQueue<>();
        ExecutorService svc = Executors.newFixedThreadPool(input.length() + 1);
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            svc.submit(() -> {
                if (c == 'H') {
                    try {
                        lc.hydrogen(() -> q.add(c));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }

                } else {
                    try {
                        lc.oxygen(() -> q.add(c));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }

                }

            });
        }


        try {
            svc.shutdown();
            svc.awaitTermination(10L, TimeUnit.SECONDS);
            String ordered = q.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
            System.out.println(ordered);
            check(input.length(), ordered);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void check(int length, String result) {
        Assertions.assertEquals(length, result.length());
        int hcnt = 0;
        int ocnt = 0;
        for (int i = 0; i < result.length(); i++) {
            char c = result.charAt(i);
            if (c == 'H') {
                hcnt++;
            } else {
                ocnt++;
            }
            if ((i + 1) % 3 == 0) {
                Assertions.assertEquals(2, hcnt);
                Assertions.assertEquals(1, ocnt);
                hcnt = 0;
                ocnt = 0;
            }
        }
    }
}
