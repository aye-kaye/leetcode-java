package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class LC284Test {

    @Test
    public void test() {
        List<Integer> list = List.of(1, 2, 3);
        LC284 lc = new LC284(list.iterator());
        Assertions.assertEquals(1, lc.next());
        Assertions.assertEquals(2, lc.peek());
        Assertions.assertEquals(2, lc.next());
        Assertions.assertEquals(3, lc.next());
        Assertions.assertFalse(lc.hasNext());
    }
}
