package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LC1544Test {
    @Test
    public void test() {
        LC1544 lc = new LC1544();
        Assertions.assertEquals("", lc.makeGood("aAbBcC"));
        Assertions.assertEquals("leetcode", lc.makeGood("leEeetcode"));
        Assertions.assertEquals("", lc.makeGood("abBAcC"));
        Assertions.assertEquals("", lc.makeGood("Pp"));
    }
}
