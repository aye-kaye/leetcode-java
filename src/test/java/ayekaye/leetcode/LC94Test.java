package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class LC94Test {
    @Test
    public void test() {
        LC94 runner = new LC94();
        {
            LC94.TreeNode test3 = new LC94.TreeNode(3);
            LC94.TreeNode test2 = new LC94.TreeNode(2, test3, null);
            LC94.TreeNode test1 = new LC94.TreeNode(1, null, test2);

            Integer[] test3arr = runner.inorderTraversal(test1).toArray(new Integer[]{});
            System.out.println(Arrays.asList(test3arr));
            Assertions.assertArrayEquals(new Integer[] {1, 3, 2}, test3arr);
        }
        {
            Integer[] test0arr = runner.inorderTraversal(null).toArray(new Integer[]{});
            Assertions.assertArrayEquals(new Integer[] {}, test0arr);
        }
        {
            LC94.TreeNode test1 = new LC94.TreeNode(1, null, null);
            Integer[] test1arr = runner.inorderTraversal(test1).toArray(new Integer[]{});
            Assertions.assertArrayEquals(new Integer[] {1}, test1arr);
        }
        {
            LC94.TreeNode testm1 = new LC94.TreeNode(-1);
            LC94.TreeNode test1 = new LC94.TreeNode(1);
            LC94.TreeNode test0 = new LC94.TreeNode(0, testm1, test1);

            Integer[] test0arr = runner.inorderTraversal(test0).toArray(new Integer[]{});
            System.out.println(Arrays.asList(test0arr));
            Assertions.assertArrayEquals(new Integer[] {-1, 0, 1}, test0arr);
        }
        {
            LC94.TreeNode test1 = new LC94.TreeNode(1);
            LC94.TreeNode test3 = new LC94.TreeNode(3, test1, null);
            LC94.TreeNode test2 = new LC94.TreeNode(2, test3, null);

            Integer[] test2arr = runner.inorderTraversal(test2).toArray(new Integer[]{});
            System.out.println(Arrays.asList(test2arr));
            Assertions.assertArrayEquals(new Integer[] {1, 3, 2}, test2arr);
        }
    }
}
