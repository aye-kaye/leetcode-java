package ayekaye.leetcode;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;

public class LC2092Test {
    @Test
    public void test() {
        int ts = 5;
        int n = 10;
        Random r = new Random();
        int[][] meetings = new int[n * n][3];
        int pos = 0;
        for (int x = 0; x < n; x++) {
            for (int y = 0; y < n; y++) {
                if (x == y) {
                    continue;
                }
                meetings[pos++] = new int[] { x, y, r.nextInt(ts) };
            }
        }
        List<Integer> res = new LC2092().findAllPeople(n, meetings, ts);
        System.out.println(res);
    }
}
