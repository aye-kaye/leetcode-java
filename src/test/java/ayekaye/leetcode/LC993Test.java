package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LC993Test {
    @Test
    public void test1() {
        LC993 lc = new LC993();
        LC993.TreeNode root = new LC993.TreeNode(1,
            new LC993.TreeNode(2,
                new LC993.TreeNode(4),
                null
            ),
            new LC993.TreeNode(3)
        );
        Assertions.assertFalse(lc.isCousins(root, 4, 3));
    }

    @Test
    public void test2() {
        LC993 lc = new LC993();
        LC993.TreeNode root = new LC993.TreeNode(1,
            new LC993.TreeNode(2,
                null,
                new LC993.TreeNode(4)
            ),
            new LC993.TreeNode(3,
                null,
                new LC993.TreeNode(5)
            )
        );
        Assertions.assertTrue(lc.isCousins(root, 5, 4));
    }

    @Test
    public void test3() {
        LC993 lc = new LC993();
        LC993.TreeNode root = new LC993.TreeNode(1,
                new LC993.TreeNode(2,
                        null,
                        new LC993.TreeNode(4)
                ),
                new LC993.TreeNode(3)
        );
        Assertions.assertFalse(lc.isCousins(root, 2, 3));
    }

    @Test
    public void test4() {
        LC993 lc = new LC993();
        LC993.TreeNode root = new LC993.TreeNode(1,
                new LC993.TreeNode(2),
                new LC993.TreeNode(3,
                    new LC993.TreeNode(4),
                    new LC993.TreeNode(5)
                )
        );
        Assertions.assertFalse(lc.isCousins(root, 4, 5));
    }
}
