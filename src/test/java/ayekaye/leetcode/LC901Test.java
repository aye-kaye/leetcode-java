package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class LC901Test {
    @Test
    public void runner() {
        LC901 lc = new LC901();
        Integer[] input = {100, 80, 60, 70, 60, 75, 85};
        Integer[] answer = {1, 1, 1, 2, 1, 4, 6};
        Integer[] spans = Arrays.stream(input).map(lc::next).toArray(Integer[]::new);

        Assertions.assertArrayEquals(answer, spans);
    }

}
