package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LC219Test {
    @Test
    public void testAll() {
        LC219 lc = new LC219();
        int[] in20 = TestUtils.readNumsFile("src/test/resources/LC219-sample20.txt");
        boolean r20 = lc.containsNearbyDuplicate(in20, 35_000);
        Assertions.assertFalse(r20);
    }
}
