package ayekaye.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LC1116Test {
    @Test
    public void test() {
        int[] tests = {5, 7};
        for (int n : tests) {
            StringBuffer sb = new StringBuffer();
            IntConsumer c = sb::append;
            final LC1116 o = new LC1116(n);
            Thread t1 = new Thread(() -> {
                try {
                    o.zero(c);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            Thread t2 = new Thread(() -> {
                try {
                    o.even(c);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            Thread t3 = new Thread(() -> {
                try {
                    o.odd(c);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });

            t1.start();
            t2.start();
            t3.start();

            try {
                t1.join();
                t2.join();
                t3.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Assertions.assertEquals(
                    IntStream.rangeClosed(1, n).mapToObj(i -> String.format("0%d", i)).collect(Collectors.joining()),
                    sb.toString());
        }
    }
}
