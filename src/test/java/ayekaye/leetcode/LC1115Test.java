package ayekaye.leetcode;

import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class LC1115Test {
    @Test
    public void test() {
        int n = 5;
        final StringBuffer sb = new StringBuffer();
        Runnable printFoo = () -> sb.append("foo");
        Runnable printBar = () -> sb.append("bar");
        LC1115 o1 = new LC1115(n);
        LC1115 o2 = new LC1115(n);
        Thread t1 = new Thread(() -> {
            try {
                o1.foo(printFoo);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                o2.bar(printBar);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(IntStream.range(0, n).mapToObj(i -> "foobar").collect(Collectors.joining()), sb.toString());
    }
}
